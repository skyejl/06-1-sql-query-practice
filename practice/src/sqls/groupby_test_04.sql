/*
 * 请告诉我总金额大于 60000 的每一个订单（`order`）编号及其总金额。查询结果应当包含如下信息：
 *
 * +──────────────+─────────────+
 * | orderNumber  | totalPrice  |
 * +──────────────+─────────────+
 *
 * 其结果应当以 `orderNumber` 排序。
 */
select * from (SELECT O.orderNumber,sum(D.quantityOrdered * D.priceEach) totalPrice
FROM orders O INNER JOIN orderdetails D ON O.orderNumber=D.orderNumber GROUP BY O.orderNumber) a where a.totalPrice > 60000;
