/*
 * 请告诉我所有订单（order）中的订单明细的最大数目、最小数目和平均数目。结果应当包含三列：
 *
 * +────────────────────+────────────────────+────────────────────+
 * | minOrderItemCount  | maxOrderItemCount  | avgOrderItemCount  |
 * +────────────────────+────────────────────+────────────────────+
 */
SELECT MIN(items) AS minOrderItemCount, MAX(items) AS maxOrderItemCount
	, FLOOR(AVG(items)) AS avgOrderItemCount
FROM (
	SELECT `orderNumber`, COUNT(`orderLineNumber`) AS items
	FROM orderdetails
	GROUP BY orderNumber
) lineItems
